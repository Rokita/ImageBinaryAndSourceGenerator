import sys

from ArgumentParser.ArgumentParser import ArgumentParser
from ImageConverter.ImageConverter import ImageConverter
from FilesFinder.FilesFinder import FilesFinder
from BinaryGenerator.BinaryGenerator import BinaryGenerator
from LocalImage.LocalImage import LocalImage
from CodeGenerator.CodeGenerator import CodeGenerator

def main(args):
    print("Image converter and Flash genrator is running. Please wait ...\n")
    #parsing arguments. 
    arguments = ArgumentParser().parsArguments(args)
    
    images_directory = arguments.images_dir
    #create generator of binary file
    binary_generator = BinaryGenerator(arguments.output_path)
    #create generator of source and header files
    code_generator = CodeGenerator(arguments.output_path)
    #find all files with extension ".png" and ".jpg" in "image_directory"
    list_of_images = FilesFinder().find(images_directory, (".png", ".jpg"))
    for image_file in list_of_images:
        img = LocalImage(image_file)
        image_array = ImageConverter().convertToArrayRGB565(img)
        binary_generator.addArray(image_array)
        code_generator.addImage(img, 2)
    code_generator.generate()
        
    print("Process finished successfully.")    
    pass

if __name__ == "__main__":
    
    main(sys.argv[1:])
    pass
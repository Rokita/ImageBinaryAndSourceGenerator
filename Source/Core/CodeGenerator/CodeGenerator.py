
import os

bitmap_enum_prefix = "deBitmapId_"
bitmap_object_prefix = "oBitmap_"


class Bitmap:

    def __init__(self, name, width, height, pixel_size):
        self._name = name
        self._width = width
        self._height = height
        self._pixel_size = pixel_size
        self._id_prefix = "deBitmapId_"
        self._object_prefix = "oBitmap_"
        pass
    
    def __eq__(self, other):
        return self._name == other._name
        pass
    
    def getName(self):
        return self._name
        pass
    
    def getWidth(self):
        return self._width
        pass
    
    def getHeight(self):
        return self._height
        pass
    
    def __str__(self):
        return "name: {0} \n  width: {1} \n  height: {2} \n  pixel_size: {3}".format(self._name, self._width, self._height, self._pixel_size)
        pass

    pass


class CodeGenerator:

    def __init__(self, file_path):
        self._bitmaps = []
        self._duplicated_bitmaps = []
        self._bitmap_id_file_name = file_path + "/Bitmaps/" + "BitmapId.h"
        self._bitmap_database_header_file_name = file_path + "/Bitmaps/" + "BitmapDatabase.h"
        self._bitmap_database_source_file_name = file_path + "/Bitmaps/" + "BitmapDatabase.c"
        os.makedirs(os.path.dirname(self._bitmap_id_file_name), exist_ok=True)
        pass
    
    def addImage(self, img, pixel_size):
        self._pixel_size_in_bytes = pixel_size
        bitmap = Bitmap(img.getImageName(), img.getImageWidth(), img.getImageHeight(), self._pixel_size_in_bytes)
        if bitmap in self._bitmaps:
            self._duplicated_bitmaps.append(bitmap)
        else:
            self._bitmaps.append(bitmap)
        pass
    
    def generate(self):
        self.__generateBitmapIdFile()
        self.__generateBitmapDatabaseHeader()
        self.__generateBitmapDatabaseSource()
        pass
    
    def __generateBitmapIdFile(self):
        with open(self._bitmap_id_file_name, "w") as file: 
            file.write("#ifndef BITMAP_ID_H\n")
            file.write("#define BITMAP_ID_H\n")
            file.write("#ifdef __cplusplus\n")
            file.write("extern \"C\"\n")
            file.write("{\n")
            file.write("#endif\n")
            file.write("typedef enum\n")
            file.write("   {\n")
            
            enum_postfix = " = 0,"
            for bitmap in self._bitmaps:
                file.write("   {0}{1}{2}\n".format(bitmap_enum_prefix, bitmap.getName(), enum_postfix))
                enum_postfix = ","
            file.write("   {0}NumOf\n".format(bitmap_enum_prefix))                 
            file.write("   }BitmapId_e;\n")
            
            file.write("#ifdef __cplusplus\n")
            file.write("}\n")
            file.write("#endif\n")
            file.write("#endif /*BITMAP_ID_H*/\n")
        pass
        
    def __generateBitmapDatabaseHeader(self):
        with open(self._bitmap_database_header_file_name, "w") as file: 
            file.write("#ifndef BITMAP_DATABASE_H\n")
            file.write("#define BITMAP_DATABASE_H\n")
            file.write("#ifdef __cplusplus\n")
            file.write("extern \"C\"\n")
            file.write("{\n")
            file.write("#endif\n")
            
            file.write("#include <stdint.h>\n")
            file.write("#include \"BitmapId.h\"\n\n")
            
            file.write("uint16_t GetBitmapWidth(const BitmapId_e eBitmapId);\n")
            file.write("uint16_t GetBitmapHeight(const BitmapId_e eBitmapId);\n")
            file.write("uint32_t GetBitmapSizeInBytes(const BitmapId_e eBitmapId);\n")
            file.write("uint32_t GetBitmapAddressStart(const BitmapId_e eBitmapId);\n")
            file.write("uint32_t GetBitmapLineAddressStart(const BitmapId_e eBitmapId, const uint32_t u32Line);\n")

            file.write("#ifdef __cplusplus\n")
            file.write("}\n")
            file.write("#endif\n")
            file.write("#endif /*BITMAP_DATABASE_H*/\n")
        pass
      
    def __generateBitmapDatabaseSource(self):
        with open(self._bitmap_database_source_file_name, "w") as file: 
            file.write("#include <dAssert.h>\n")
            file.write("#include \"BitmapDatabase.h\"\n")
            file.write("static const uint16_t PIXEL_SIZE_IN_BYTES = {0};\n\n".format(self._pixel_size_in_bytes))
            file.write("typedef struct\n")
            file.write("   {\n")
            file.write("   uint16_t u16Width;\n")
            file.write("   uint16_t u16Height;\n")
            file.write("   uint32_t u32MemoryAddressStart;\n")
            file.write("   } BitmapStruct_t;\n")
            
            address = 0
            for bitmap in self._bitmaps:
                width = bitmap.getWidth()
                height = bitmap.getHeight()
                file.write("static const BitmapStruct_t {0}{1} =\n".format(bitmap_object_prefix, bitmap.getName()))
                file.write("   {\n")
                file.write("   {0}, {1}, {2}\n".format(width, height, address))
                file.write("   };\n")
                address += width * height * self._pixel_size_in_bytes
            file.write("/* memory end at address {0}*/\n".format(address))
            
            file.write("static BitmapStruct_t const * const TableOfBitmaps[{0}NumOf] = \n".format(bitmap_enum_prefix))
            file.write("   {\n")
            for bitmap in self._bitmaps:
                file.write("   [{0}{1}] = &{2}{3},\n".format(bitmap_enum_prefix, bitmap.getName(), bitmap_object_prefix, bitmap.getName()))
            file.write("   };\n")
                        
            file.write("uint16_t GetBitmapWidth(const BitmapId_e eBitmapId)\n")
            file.write("   {\n")
            file.write("   mAssert(eBitmapId < deBitmapId_NumOf);\n")
            file.write("   return TableOfBitmaps[eBitmapId]->u16Width;\n")
            file.write("   }\n")
            
            file.write("uint16_t GetBitmapHeight(const BitmapId_e eBitmapId)\n")
            file.write("   {\n")
            file.write("   mAssert(eBitmapId < deBitmapId_NumOf);\n")
            file.write("   return TableOfBitmaps[eBitmapId]->u16Height;\n")
            file.write("   }\n")
            
            file.write("uint32_t GetBitmapSizeInBytes(const BitmapId_e eBitmapId)\n")
            file.write("   {\n")
            file.write("   mAssert(eBitmapId < deBitmapId_NumOf);\n")
            file.write("   return (GetBitmapHeight(eBitmapId) * GetBitmapWidth(eBitmapId) * PIXEL_SIZE_IN_BYTES);\n")
            file.write("   }\n")
            
            file.write("uint32_t GetBitmapAddressStart(const BitmapId_e eBitmapId)\n")
            file.write("   {\n")
            file.write("   mAssert(eBitmapId < deBitmapId_NumOf);\n")
            file.write("   return TableOfBitmaps[eBitmapId]->u32MemoryAddressStart;\n")
            file.write("   }\n")
            
            file.write("uint32_t GetBitmapLineAddressStart(const BitmapId_e eBitmapId, const uint32_t u32Line)\n")
            file.write("   {\n")
            file.write("   mAssert(eBitmapId < deBitmapId_NumOf);\n")
            file.write("   mAssert(u32Line < TableOfBitmaps[eBitmapId]->u16Height);\n")
            file.write("   uint32_t u32ReturnAddress = TableOfBitmaps[eBitmapId]->u32MemoryAddressStart + (u32Line * TableOfBitmaps[eBitmapId]->u16Width * PIXEL_SIZE_IN_BYTES);\n")
            file.write("   return u32ReturnAddress;\n")
            file.write("   }\n")
            
        pass
    
    def __del__(self):
        if len(self._duplicated_bitmaps) != 0:
            print("Duplicated Bitmaps")    
            for bitmap in self._duplicated_bitmaps:
                print(bitmap)
        pass
    
    pass

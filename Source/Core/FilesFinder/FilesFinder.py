import os


class FilesFinder:
    
    def __init__(self):
        pass

    def find(self, directory_path, extensions):
        list_of_files = []
        absolute_path = os.path.abspath(directory_path)
        if os.path.isdir(absolute_path):
            print("Searching images in directory: {0}.".format(absolute_path))
            for root, dirs, files in os.walk(absolute_path):
                for file in files:
                    if file.endswith(extensions):
                        list_of_files.append(os.path.join(os.path.abspath(root), file))
        else: 
            print("{0} is not a valid directory.".format(absolute_path))
        return list_of_files
        pass
    
    pass


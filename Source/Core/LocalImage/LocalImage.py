import os.path
from PIL import Image as Img


class LocalImage:

    def __init__(self, image_path):
        self._image_file = None
        self._image_path = ""
        self._image_name = ""
        self._image_extension = ""
        self._image_width = 0
        self._image_height = 0
        if image_path and os.path.isfile(image_path):
            self._image_file = Img.open(image_path, mode='r')
            self._image_path = image_path
            self._image_name, self._image_extension = image_path.split("\\")[-1].split(".")
            self._image_name = self._image_name.replace(" ", "_")
            self._image_name = self._image_name.replace("-", "_")
            self._image_width, self._image_height = self._image_file.size
        else: 
            print("{0} is not an image file.".format(image_path))
            return None
        pass
    
    def getPilImage(self):
        return self._image_file
        pass
    
    def getImagePath(self):
        return self._image_path
        pass
    
    def getImageName(self):
        return self._image_name
        pass

    def getImageExtension(self):
        return self._image_extension
        pass
    
    def getImageWidth(self):
        return self._image_width
        pass
    
    def getImageHeight(self):
        return self._image_height
        pass
    
    def __str__(self):
        return "path: {0} \n  name: {1} \n  extension: {2} \n  width: {3} \n  height: {4}".format(self._image_path, self._image_name, self._image_extension, self._image_width, self._image_height)
        pass

    def __del__(self):
        if self._image_file is not None:
            self._image_file.close()
        pass

    pass

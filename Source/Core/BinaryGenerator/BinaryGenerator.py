
import os


class BinaryGenerator:

    def __init__(self, file_path):
        self._file_name = file_path + "/" + "BinaryFile.bin"
        os.makedirs(os.path.dirname(self._file_name), exist_ok=True)
        self._file = open(self._file_name, "wb")
        pass

    def addArray(self, array):
        if self._file is not None: 
            print("     Binary generation")
            for line in array:
                self._file.write(bytearray(line))
        pass
    
    def __del__(self):
        if self._file is not None: 
            self._file.close()
        pass

    pass

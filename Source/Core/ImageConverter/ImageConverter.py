from ImageConverter.ImageConvertionFunctions.ImageConvertionFunctions import ImageConvertionFunctions

class ImageConverter:
    def __init__(self):
        pass
    
    def convertToArrayRGB565(self, image_path):
        return self._openImageAndConvert(image_path, ImageConvertionFunctions().toRGB565)
        pass
    
    def convertToArrayRGB565Swap(self, image_path):
        return self._openImageAndConvert(image_path, ImageConvertionFunctions().toRGB565Swap)
        pass
    
    def _openImageAndConvert(self, image, conversion_function):
        converted_image = None
        if image is not None:
            print("Opening file: {0}".format(image.getImagePath()))
            print("     Image conversion")
            converted_image = conversion_function(image.getPilImage())
        else: 
            print("It is not an image.")
        return converted_image
        pass
    pass

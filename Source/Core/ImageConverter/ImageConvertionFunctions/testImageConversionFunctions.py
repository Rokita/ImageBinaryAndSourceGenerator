import unittest
from numpy import asarray
from PIL import Image

from ImageConverter.ImageConvertionFunctions.ImageConvertionFunctions import ImageConvertionFunctions


class testImageConversionFunctions(unittest.TestCase):
    
    def testImage1x1WhiteConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,1), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        pass
    
    def testImage1x2WhiteConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,2), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        self.assertEqual(image_rgb565[1][0], 0xFF)
        self.assertEqual(image_rgb565[1][1], 0xFF)
        pass
    
    def testImage2x1WhiteConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,1), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        self.assertEqual(image_rgb565[0][2], 0xFF)
        self.assertEqual(image_rgb565[0][3], 0xFF)
        pass

    def testImage2x2WhiteConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,2), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        self.assertEqual(image_rgb565[0][2], 0xFF)
        self.assertEqual(image_rgb565[0][3], 0xFF)
        self.assertEqual(image_rgb565[1][0], 0xFF)
        self.assertEqual(image_rgb565[1][1], 0xFF)
        self.assertEqual(image_rgb565[1][2], 0xFF)
        self.assertEqual(image_rgb565[1][3], 0xFF)
        pass


    
    def testImage1x1RedConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,1), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xF8)
        self.assertEqual(image_rgb565[0][1], 0x00)
        pass
    
    def testImage1x2RedConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,2), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xF8)
        self.assertEqual(image_rgb565[0][1], 0x00)
        self.assertEqual(image_rgb565[1][0], 0xF8)
        self.assertEqual(image_rgb565[1][1], 0x00)
        pass
    
    def testImage2x1RedConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,1), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xF8)
        self.assertEqual(image_rgb565[0][1], 0x00)
        self.assertEqual(image_rgb565[0][2], 0xF8)
        self.assertEqual(image_rgb565[0][3], 0x00)
        pass

    def testImage2x2RedConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,2), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xF8)
        self.assertEqual(image_rgb565[0][1], 0x00)
        self.assertEqual(image_rgb565[0][2], 0xF8)
        self.assertEqual(image_rgb565[0][3], 0x00)
        self.assertEqual(image_rgb565[1][0], 0xF8)
        self.assertEqual(image_rgb565[1][1], 0x00)
        self.assertEqual(image_rgb565[1][2], 0xF8)
        self.assertEqual(image_rgb565[1][3], 0x00)
        pass



    
    def testImage1x1GreenConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,1), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x07)
        self.assertEqual(image_rgb565[0][1], 0xE0)
        pass
    
    def testImage1x2GreenConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,2), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x07)
        self.assertEqual(image_rgb565[0][1], 0xE0)
        self.assertEqual(image_rgb565[1][0], 0x07)
        self.assertEqual(image_rgb565[1][1], 0xE0)
        pass
    
    def testImage2x1GreenConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,1), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x07)
        self.assertEqual(image_rgb565[0][1], 0xE0)
        self.assertEqual(image_rgb565[0][2], 0x07)
        self.assertEqual(image_rgb565[0][3], 0xE0)
        pass

    def testImage2x2GreenConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,2), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x07)
        self.assertEqual(image_rgb565[0][1], 0xE0)
        self.assertEqual(image_rgb565[0][2], 0x07)
        self.assertEqual(image_rgb565[0][3], 0xE0)
        self.assertEqual(image_rgb565[1][0], 0x07)
        self.assertEqual(image_rgb565[1][1], 0xE0)
        self.assertEqual(image_rgb565[1][2], 0x07)
        self.assertEqual(image_rgb565[1][3], 0xE0)
        pass


    
    def testImage1x1BlueConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,1), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0x1F)
        pass
    
    def testImage1x2BlueConversionToRGB565(self):
        pil_image = Image.new("RGB", (1,2), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0x1F)
        self.assertEqual(image_rgb565[1][0], 0x00)
        self.assertEqual(image_rgb565[1][1], 0x1F)
        pass
    
    def testImage2x1BlueConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,1), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0x1F)
        self.assertEqual(image_rgb565[0][2], 0x00)
        self.assertEqual(image_rgb565[0][3], 0x1F)
        pass

    def testImage2x2BlueConversionToRGB565(self):
        pil_image = Image.new("RGB", (2,2), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0x1F)
        self.assertEqual(image_rgb565[0][2], 0x00)
        self.assertEqual(image_rgb565[0][3], 0x1F)
        self.assertEqual(image_rgb565[1][0], 0x00)
        self.assertEqual(image_rgb565[1][1], 0x1F)
        self.assertEqual(image_rgb565[1][2], 0x00)
        self.assertEqual(image_rgb565[1][3], 0x1F)
        pass


#     
#    Conversion to RGB565 Swap
#


    def testImage1x1WhiteConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,1), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        pass
    
    def testImage1x2WhiteConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,2), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        self.assertEqual(image_rgb565[1][0], 0xFF)
        self.assertEqual(image_rgb565[1][1], 0xFF)
        pass
    
    def testImage2x1WhiteConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,1), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        self.assertEqual(image_rgb565[0][2], 0xFF)
        self.assertEqual(image_rgb565[0][3], 0xFF)
        pass

    def testImage2x2WhiteConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,2), '#FFFFFF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xFF)
        self.assertEqual(image_rgb565[0][1], 0xFF)
        self.assertEqual(image_rgb565[0][2], 0xFF)
        self.assertEqual(image_rgb565[0][3], 0xFF)
        self.assertEqual(image_rgb565[1][0], 0xFF)
        self.assertEqual(image_rgb565[1][1], 0xFF)
        self.assertEqual(image_rgb565[1][2], 0xFF)
        self.assertEqual(image_rgb565[1][3], 0xFF)
        pass


    
    def testImage1x1RedConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,1), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0xF8)
        pass
    
    def testImage1x2RedConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,2), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0xF8)
        self.assertEqual(image_rgb565[1][0], 0x00)
        self.assertEqual(image_rgb565[1][1], 0xF8)
        pass
    
    def testImage2x1RedConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,1), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0xF8)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0xF8)
        pass

    def testImage2x2RedConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,2), '#FF0000')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x00)
        self.assertEqual(image_rgb565[0][1], 0xF8)
        self.assertEqual(image_rgb565[0][2], 0x00)
        self.assertEqual(image_rgb565[0][3], 0xF8)
        self.assertEqual(image_rgb565[1][0], 0x00)
        self.assertEqual(image_rgb565[1][1], 0xF8)
        self.assertEqual(image_rgb565[1][2], 0x00)
        self.assertEqual(image_rgb565[1][3], 0xF8)
        pass



    
    def testImage1x1GreenConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,1), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xE0)
        self.assertEqual(image_rgb565[0][1], 0x07)
        pass
    
    def testImage1x2GreenConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,2), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xE0)
        self.assertEqual(image_rgb565[0][1], 0x07)
        self.assertEqual(image_rgb565[1][0], 0xE0)
        self.assertEqual(image_rgb565[1][1], 0x07)
        pass
    
    def testImage2x1GreenConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,1), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xE0)
        self.assertEqual(image_rgb565[0][1], 0x07)
        self.assertEqual(image_rgb565[0][0], 0xE0)
        self.assertEqual(image_rgb565[0][1], 0x07)
        pass

    def testImage2x2GreenConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,2), '#00FF00')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0xE0)
        self.assertEqual(image_rgb565[0][1], 0x07)
        self.assertEqual(image_rgb565[0][2], 0xE0)
        self.assertEqual(image_rgb565[0][3], 0x07)
        self.assertEqual(image_rgb565[1][0], 0xE0)
        self.assertEqual(image_rgb565[1][1], 0x07)
        self.assertEqual(image_rgb565[1][2], 0xE0)
        self.assertEqual(image_rgb565[1][3], 0x07)
        pass


    
    def testImage1x1BlueConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,1), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x1F)
        self.assertEqual(image_rgb565[0][1], 0x00)
        pass
    
    def testImage1x2BlueConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (1,2), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x1F)
        self.assertEqual(image_rgb565[0][1], 0x00)
        self.assertEqual(image_rgb565[1][0], 0x1F)
        self.assertEqual(image_rgb565[1][1], 0x00)
        pass
    
    def testImage2x1BlueConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,1), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x1F)
        self.assertEqual(image_rgb565[0][1], 0x00)
        self.assertEqual(image_rgb565[0][2], 0x1F)
        self.assertEqual(image_rgb565[0][3], 0x00)
        pass

    def testImage2x2BlueConversionToRGB565Swap(self):
        pil_image = Image.new("RGB", (2,2), '#0000FF')
        image_rgb565 = ImageConvertionFunctions().toRGB565Swap(pil_image)
        self.assertEqual(image_rgb565[0][0], 0x1F)
        self.assertEqual(image_rgb565[0][1], 0x00)
        self.assertEqual(image_rgb565[0][2], 0x1F)
        self.assertEqual(image_rgb565[0][3], 0x00)
        self.assertEqual(image_rgb565[1][0], 0x1F)
        self.assertEqual(image_rgb565[1][1], 0x00)
        self.assertEqual(image_rgb565[1][2], 0x1F)
        self.assertEqual(image_rgb565[1][3], 0x00)
        pass



    pass


class PixelConverter: 
    
    def __init__(self):
        pass
    
    def convertRGB888ToRGB565(self, pixel_rgb888):
        firstByte = pixel_rgb888[0] & 0xF8
        firstByte |= (pixel_rgb888[1] >> 5)
        secondByte = (pixel_rgb888[1] << 3) & 0xE0
        secondByte |= (pixel_rgb888[2] >> 3)
        rgb_565 = [firstByte, secondByte]
        return rgb_565
        pass
    
    def convertRGB888ToRGB565Swap(self, pixel_rgb888):
        rgb_565 = self.convertRGB888ToRGB565(pixel_rgb888)
        rgb_565Swap = [rgb_565[1], rgb_565[0]]
        return rgb_565Swap
        pass
    
    pass
import unittest
from ImageConverter.ImageConvertionFunctions.PixelConverter.PixelConverter import PixelConverter

class testPixelConverter(unittest.TestCase):
    
    def testConvertWhitePixelRGB888ToRGB565(self):
        white_rgb888 = [0xFF, 0xFF, 0xFF]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565(white_rgb888)
        self.assertEqual(white_rgb565[0], 0xFF)
        self.assertEqual(white_rgb565[1], 0xFF)
        pass
    
    def testConvertRedPixelRGB888ToRGB565(self):
        white_rgb888 = [0xFF, 0x00, 0x00]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565(white_rgb888)
        self.assertEqual(white_rgb565[0], 0xF8)
        self.assertEqual(white_rgb565[1], 0x00)
        pass
    
    def testConvertGreenPixelRGB888ToRGB565(self):
        white_rgb888 = [0x00, 0xFF, 0x00]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565(white_rgb888)
        self.assertEqual(white_rgb565[0], 0x07)
        self.assertEqual(white_rgb565[1], 0xE0)
        pass
    
    def testConvertBluePixelRGB888ToRGB565(self):
        white_rgb888 = [0x00, 0x00, 0xFF]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565(white_rgb888)
        self.assertEqual(white_rgb565[0], 0x00)
        self.assertEqual(white_rgb565[1], 0x1F)
        pass
    
    ###########################################################
    ##                RGB888 To RGB565Swap                   ##
    ###########################################################
        
    def testConvertWhitePixelRGB888ToRGB565Swap(self):
        white_rgb888 = [0xFF, 0xFF, 0xFF]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565Swap(white_rgb888)
        self.assertEqual(white_rgb565[0], 0xFF)
        self.assertEqual(white_rgb565[1], 0xFF)
        pass
    
    def testConvertRedPixelRGB888ToRGB565Swap(self):
        white_rgb888 = [0xFF, 0x00, 0x00]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565Swap(white_rgb888)
        self.assertEqual(white_rgb565[0], 0x00)
        self.assertEqual(white_rgb565[1], 0xF8)
        pass
    
    def testConvertGreenPixelRGB888ToRGB565Swap(self):
        white_rgb888 = [0x00, 0xFF, 0x00]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565Swap(white_rgb888)
        self.assertEqual(white_rgb565[0], 0xE0)
        self.assertEqual(white_rgb565[1], 0x07)
        pass
    
    def testConvertBluePixelRGB888ToRGB565Swap(self):
        white_rgb888 = [0x00, 0x00, 0xFF]
        white_rgb565 = PixelConverter().convertRGB888ToRGB565Swap(white_rgb888)
        self.assertEqual(white_rgb565[0], 0x1F)
        self.assertEqual(white_rgb565[1], 0x00)
        pass
    
    pass
from numpy import asarray
import numpy.core._dtype_ctypes

from ImageConverter.ImageConvertionFunctions.PixelConverter.PixelConverter import PixelConverter

class ImageConvertionFunctions:
    def __init__(self):
        pass
    
    def toRGB565(self, pil_image):
        data_array = asarray(pil_image)
        rgb_array = []
        for line_data in data_array: 
            rgb_line = []
            for pixel_color in line_data:
                rgb_565 = PixelConverter().convertRGB888ToRGB565(pixel_color)
                rgb_line.append(rgb_565[0])
                rgb_line.append(rgb_565[1])
            rgb_array.append(rgb_line)
        return rgb_array
        pass
    
    def toRGB565Swap(self, pil_image):
        data_array = asarray(pil_image)
        rgb_array = []
        for line_data in data_array: 
            rgb_line = []
            for pixel_color in line_data:
                rgb_565 = PixelConverter().convertRGB888ToRGB565Swap(pixel_color)
                rgb_line.append(rgb_565[0])
                rgb_line.append(rgb_565[1])
            rgb_array.append(rgb_line)
        return rgb_array
        pass
    
    pass

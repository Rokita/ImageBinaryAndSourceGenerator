import unittest
from PIL import Image
import os

from ImageConverter.ImageConverter import ImageConverter
from LocalImage.LocalImage import LocalImage

class testImageConverterConverter(unittest.TestCase):
    img = None
    
    def tearDown(self):
        if self.img and os.path.isfile(self.img.getImagePath()):
            os.remove(self.img.getImagePath())
        self.img = None
        pass
        
    def __createImage(self, width, height, color, name):
        pil_image = Image.new("RGB", (width, height), color)
        pil_image.save(name)
        self.img = LocalImage(name)
        pass
    
    def testImageConverterWhenNoneIsSendtoConversionThenNoneShalBeReturned(self):
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565, None)      
        pass
    
    def testImageConverter1x1WhiteImageToRGB565(self):
        self.__createImage(1, 1, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)      
        self.assertEqual(array_rgb565[0][1], 0xFF)
        pass
    
    def testImageConverter1x2WhiteImageToRGB565(self):
        self.__createImage(1, 2, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)
        self.assertEqual(array_rgb565[0][1], 0xFF)
        self.assertEqual(array_rgb565[1][0], 0xFF)
        self.assertEqual(array_rgb565[1][1], 0xFF)
        pass
    
    def testImageConverter2x1WhiteImageToRGB565(self):
        self.__createImage(2, 1, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)
        self.assertEqual(array_rgb565[0][1], 0xFF)
        self.assertEqual(array_rgb565[0][2], 0xFF)
        self.assertEqual(array_rgb565[0][3], 0xFF)
        pass

    def testImageConverter2x2WhiteImageToRGB565(self):
        self.__createImage(2, 2, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)
        self.assertEqual(array_rgb565[0][1], 0xFF)
        self.assertEqual(array_rgb565[0][2], 0xFF)
        self.assertEqual(array_rgb565[0][3], 0xFF)
        self.assertEqual(array_rgb565[1][0], 0xFF)
        self.assertEqual(array_rgb565[1][1], 0xFF)
        self.assertEqual(array_rgb565[1][2], 0xFF)
        self.assertEqual(array_rgb565[1][3], 0xFF)
        pass

    def testImageConverter1x1RedImageToRGB565(self):
        self.__createImage(1, 1, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xF8)
        self.assertEqual(array_rgb565[0][1], 0x00)
        pass
    
    def testImageConverter1x2RedImageToRGB565(self):
        self.__createImage(1, 2, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xF8)
        self.assertEqual(array_rgb565[0][1], 0x00)
        self.assertEqual(array_rgb565[1][0], 0xF8)
        self.assertEqual(array_rgb565[1][1], 0x00)
        pass
    
    def testImageConverter2x1RedImageToRGB565(self):
        self.__createImage(2, 1, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xF8)
        self.assertEqual(array_rgb565[0][1], 0x00)
        self.assertEqual(array_rgb565[0][2], 0xF8)
        self.assertEqual(array_rgb565[0][3], 0x00)
        pass

    def testImageConverter2x2RedImageToRGB565(self):
        self.__createImage(2, 2, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0xF8)
        self.assertEqual(array_rgb565[0][1], 0x00)
        self.assertEqual(array_rgb565[0][2], 0xF8)
        self.assertEqual(array_rgb565[0][3], 0x00)
        self.assertEqual(array_rgb565[1][0], 0xF8)
        self.assertEqual(array_rgb565[1][1], 0x00)
        self.assertEqual(array_rgb565[1][2], 0xF8)
        self.assertEqual(array_rgb565[1][3], 0x00)
        pass
    
    def testImageConverter1x1GreenImageToRGB565(self):
        self.__createImage(1, 1, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x07)
        self.assertEqual(array_rgb565[0][1], 0xE0)
        pass
    
    def testImageConverter1x2GreenImageToRGB565(self):
        self.__createImage(1, 2, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x07)
        self.assertEqual(array_rgb565[0][1], 0xE0)
        self.assertEqual(array_rgb565[1][0], 0x07)
        self.assertEqual(array_rgb565[1][1], 0xE0)
        pass
    
    def testImageConverter2x1GreenImageToRGB565(self):
        self.__createImage(2, 1, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x07)
        self.assertEqual(array_rgb565[0][1], 0xE0)
        self.assertEqual(array_rgb565[0][2], 0x07)
        self.assertEqual(array_rgb565[0][3], 0xE0)
        pass

    def testImageConverter2x2GreenImageToRGB565(self):
        self.__createImage(2, 2, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x07)
        self.assertEqual(array_rgb565[0][1], 0xE0)
        self.assertEqual(array_rgb565[0][2], 0x07)
        self.assertEqual(array_rgb565[0][3], 0xE0)
        self.assertEqual(array_rgb565[1][0], 0x07)
        self.assertEqual(array_rgb565[1][1], 0xE0)
        self.assertEqual(array_rgb565[1][2], 0x07)
        self.assertEqual(array_rgb565[1][3], 0xE0)
        pass
    
    def testImageConverter1x1BlueImageToRGB565(self):
        self.__createImage(1, 1, '#0000FF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0x1F)
        pass
    
    def testImageConverter1x2BlueImageToRGB565(self):
        self.__createImage(1, 2, '#0000FF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0x1F)
        self.assertEqual(array_rgb565[1][0], 0x00)
        self.assertEqual(array_rgb565[1][1], 0x1F)
        pass
    
    def testImageConverter2x1BlueImageToRGB565(self):
        self.__createImage(2, 1, '#0000FF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0x1F)
        self.assertEqual(array_rgb565[0][2], 0x00)
        self.assertEqual(array_rgb565[0][3], 0x1F)
        pass

    def testImageConverter2x2BlueImageToRGB565(self):
        self.__createImage(2, 2, '#0000FF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0x1F)
        self.assertEqual(array_rgb565[0][2], 0x00)
        self.assertEqual(array_rgb565[0][3], 0x1F)
        self.assertEqual(array_rgb565[1][0], 0x00)
        self.assertEqual(array_rgb565[1][1], 0x1F)
        self.assertEqual(array_rgb565[1][2], 0x00)
        self.assertEqual(array_rgb565[1][3], 0x1F)
        pass

    def testImageConverter1x1WhiteImageToRGB565Swap(self):
        self.__createImage(1, 1, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)
        self.assertEqual(array_rgb565[0][1], 0xFF)      
        pass
    
    def testImageConverter1x2WhiteImageToRGB565Swap(self):
        self.__createImage(1, 2, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)
        self.assertEqual(array_rgb565[0][1], 0xFF)
        self.assertEqual(array_rgb565[1][0], 0xFF)
        self.assertEqual(array_rgb565[1][1], 0xFF)
        pass
    
    def testImageConverter2x1WhiteImageToRGB565Swap(self):
        self.__createImage(2, 1, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)
        self.assertEqual(array_rgb565[0][1], 0xFF)
        self.assertEqual(array_rgb565[0][2], 0xFF)
        self.assertEqual(array_rgb565[0][3], 0xFF)
        pass

    def testImageConverter2x2WhiteImageToRGB565Swap(self):
        self.__createImage(2, 2, '#FFFFFF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xFF)
        self.assertEqual(array_rgb565[0][1], 0xFF)
        self.assertEqual(array_rgb565[0][2], 0xFF)
        self.assertEqual(array_rgb565[0][3], 0xFF)
        self.assertEqual(array_rgb565[1][0], 0xFF)
        self.assertEqual(array_rgb565[1][1], 0xFF)
        self.assertEqual(array_rgb565[1][2], 0xFF)
        self.assertEqual(array_rgb565[1][3], 0xFF)
        pass
    
    def testImageConverter1x1RedImageToRGB565Swap(self):
        self.__createImage(1, 1, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0xF8)
        pass
    
    def testImageConverter1x2RedImageToRGB565Swap(self):
        self.__createImage(1, 2, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0xF8)
        self.assertEqual(array_rgb565[1][0], 0x00)
        self.assertEqual(array_rgb565[1][1], 0xF8)
        pass
    
    def testImageConverter2x1RedImageToRGB565Swap(self):
        self.__createImage(2, 1, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0xF8)
        self.assertEqual(array_rgb565[0][2], 0x00)
        self.assertEqual(array_rgb565[0][3], 0xF8)
        pass

    def testImageConverter2x2RedImageToRGB565Swap(self):
        self.__createImage(2, 2, '#FF0000', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x00)
        self.assertEqual(array_rgb565[0][1], 0xF8)
        self.assertEqual(array_rgb565[0][2], 0x00)
        self.assertEqual(array_rgb565[0][3], 0xF8)
        self.assertEqual(array_rgb565[1][0], 0x00)
        self.assertEqual(array_rgb565[1][1], 0xF8)
        self.assertEqual(array_rgb565[1][2], 0x00)
        self.assertEqual(array_rgb565[1][3], 0xF8)
        pass

    def testImageConverter1x1GreenImageToRGB565Swap(self):
        self.__createImage(1, 1, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xE0)
        self.assertEqual(array_rgb565[0][1], 0x07)
        pass
    
    def testImageConverter1x2GreenImageToRGB565Swap(self):
        self.__createImage(1, 2, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xE0)
        self.assertEqual(array_rgb565[0][1], 0x07)
        self.assertEqual(array_rgb565[1][0], 0xE0)
        self.assertEqual(array_rgb565[1][1], 0x07)
        pass
    
    def testImageConverter2x1GreenImageToRGB565Swap(self):
        self.__createImage(2, 1, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xE0)
        self.assertEqual(array_rgb565[0][1], 0x07)
        self.assertEqual(array_rgb565[0][2], 0xE0)
        self.assertEqual(array_rgb565[0][3], 0x07)
        pass

    def testImageConverter2x2GreenImageToRGB565Swap(self):
        self.__createImage(2, 2, '#00FF00', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0xE0)
        self.assertEqual(array_rgb565[0][1], 0x07)
        self.assertEqual(array_rgb565[0][2], 0xE0)
        self.assertEqual(array_rgb565[0][3], 0x07)
        self.assertEqual(array_rgb565[1][0], 0xE0)
        self.assertEqual(array_rgb565[1][1], 0x07)
        self.assertEqual(array_rgb565[1][2], 0xE0)
        self.assertEqual(array_rgb565[1][3], 0x07)
        pass
    
    def testImageConverter1x1BlueImageToRGB565Swap(self):
        self.__createImage(1, 1, '#0000FF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x1F)
        self.assertEqual(array_rgb565[0][1], 0x00)
        pass
    
    def testImageConverter1x2BlueImageToRGB565Swap(self):
        self.__createImage(1, 2, '#0000FF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x1F)
        self.assertEqual(array_rgb565[0][1], 0x00)
        self.assertEqual(array_rgb565[1][0], 0x1F)
        self.assertEqual(array_rgb565[1][1], 0x00)
        pass
    
    def testImageConverter2x1BlueImageToRGB565Swap(self):
        self.__createImage(2, 1, '#0000FF', "TestedImage.png") 
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x1F)
        self.assertEqual(array_rgb565[0][1], 0x00)
        self.assertEqual(array_rgb565[0][2], 0x1F)
        self.assertEqual(array_rgb565[0][3], 0x00)
        pass

    def testImageConverter2x2BlueImageToRGB565Swap(self):
        self.__createImage(2, 2, '#0000FF', "TestedImage.png")
        array_rgb565 = ImageConverter().convertToArrayRGB565Swap(self.img)
        self.assertEqual(array_rgb565[0][0], 0x1F)
        self.assertEqual(array_rgb565[0][1], 0x00)
        self.assertEqual(array_rgb565[0][2], 0x1F)
        self.assertEqual(array_rgb565[0][3], 0x00)
        self.assertEqual(array_rgb565[1][0], 0x1F)
        self.assertEqual(array_rgb565[1][1], 0x00)
        self.assertEqual(array_rgb565[1][2], 0x1F)
        self.assertEqual(array_rgb565[1][3], 0x00)
        pass
    
    pass
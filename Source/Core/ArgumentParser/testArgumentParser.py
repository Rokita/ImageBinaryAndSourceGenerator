import unittest
from ArgumentParser.ArgumentParser import ArgumentParser

class testArgumentParser(unittest.TestCase):
    
    arg_parser = ArgumentParser()

    def testWhenNoArgumentsThenDefaultInputPath(self):
        arguments = []
        parsed = self.arg_parser.parsArguments(arguments)
        self.assertEqual(parsed.images_dir, "__DefaultImageDirectory")
        pass
    
    def testWhenNoArgumentsThenDefaultOutputPath(self):
        arguments = []
        parsed = self.arg_parser.parsArguments(arguments)
        self.assertEqual(parsed.output_path, "_Generated")
        pass
    
    def testWhenInputPathGivenAsSingleLetterOptionThenCheckIfIsCorrect(self):
        arguments = ['-i', 'MyDirectory']
        parsed = self.arg_parser.parsArguments(arguments)
        self.assertEqual(parsed.images_dir, "MyDirectory")
        pass
    
    def testWhenInputPathGivenAsFullNameOptionThenCheckIfIsCorrect(self):
        arguments = ['--input_path', 'MyDirectory']
        parsed = self.arg_parser.parsArguments(arguments)
        self.assertEqual(parsed.images_dir, "MyDirectory")
        pass
    
    def testWhenOutputPathGivenAsSingleLetterOptionThenCheckIfIsCorrect(self):
        arguments = ['-o', 'MyOutputDirectory']
        parsed = self.arg_parser.parsArguments(arguments)
        self.assertEqual(parsed.output_path, "MyOutputDirectory")
        pass
    
    def testWhenOutputPathGivenAsFullNameOptionThenCheckIfIsCorrect(self):
        arguments = ['--output_path', 'MyOutputDirectory']
        parsed = self.arg_parser.parsArguments(arguments)
        self.assertEqual(parsed.output_path, "MyOutputDirectory")
        pass
    
    pass
    
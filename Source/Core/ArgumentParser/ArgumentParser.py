import argparse


class ArgumentParser:
    __parser = argparse.ArgumentParser(description='Tool used to convert images into one flash binary file with header and sourde file in C format.')

    def __init__(self):
        self.__parser.add_argument('-i', '--input_path', dest='images_dir', help='Path to root directory of images. Tool search recursively all subfolders.')
        self.__parser.add_argument('-o', '--output_path', dest='output_path', help='Path to directory where sources and binaries will be generated.')
        pass
    
    def parsArguments(self, run_arguments):
        arguments = self.__parser.parse_args(run_arguments)
        if arguments.images_dir is None:
            arguments.images_dir = '__DefaultImageDirectory'
        if arguments.output_path is None: 
            arguments.output_path = "_Generated" 
        return arguments
        pass
    
    pass
